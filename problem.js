const fs = require('fs');
const path = require("path");
const { resolve } = require("path");
const { reject } = require("promise");


function readFile(path) {
    return new Promise((resolve, reject) => {
        fs.readFile(path, 'utf8', (err, data) => {
            if (err) {
                reject(err);
            } else {
                // console.log(data);
                resolve(data);
            }
        });
    });
}

function writeFile(path, content) {
    return new Promise((resolve, reject) => {
        fs.writeFile(path, content, 'utf8', (err) => {
            if (err) {
                reject(err);
            } else {
                // console.log(data);
                resolve(path);
            }
        });
    });
}

function appendNewLineToFile(path, line) {
    return new Promise((resolve, reject) => {
        const content = `${line.trim()}\n`;
        fs.appendFile(path, content, 'utf8', (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(content);
            }
        });
    });
}

function sortString(data) {
    return data.sort().join(` \n`);
}

function removeFile(fileName) {
    const currentremovePromise = new Promise((resolve, reject) => {
        fs.unlink(`${fileName}`, (err) => {
            if (err) {
                if (err.code == "ENOENT") {
                    // console.log("already deleted");
                    resolve();
                }
                else {
                    reject(err);
                    throw err;
                }

            }
            else {
                console.log("deleted " + fileName);
                resolve();
            }
        })
    })
    return currentremovePromise;
}

async function deleteFiles(containerFile) {
    try {
        const data = await readFile(containerFile);
        const fileNamesArray = data.split("\n");
        for (const files in fileNamesArray) {
            await removeFile(fileNamesArray[files]);
        }
        console.log("deleted all files")
        resolve();
    } catch (error) {
        console.log(error);
        reject(error);
    }


}


// functions -> do one thing and do it well

async function main() {
    try {
        // 1. Read the given file lipsum.txt
        const data = await readFile('./lipsum.txt');

        // 2.Convert the contents of the file lipsum.txt to uppercase & write the uppercase content to a new file called uppercase.txt. Store the name of the new file in filenames.txt
        const dataUpper = data.toUpperCase();
        // console.log(dataUpper);
        await writeFile('uppercase.txt', dataUpper);
        console.log('dataUpper written to uppercase.txt');

        await appendNewLineToFile('filenames.txt', 'uppercase.txt');
        console.log('uppercase.txt appended to filenames.txt');

        // 3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write each sentence into separate new files. Store the name of the new files in filenames.txt
        const uppercaseDataReadFromFile = await readFile('./uppercase.txt');
        const lowercaseData = uppercaseDataReadFromFile.toLowerCase();

        const sentences = lowercaseData.split('. ');
        const sentencesFilesPromises = sentences.map((sentence, index) => {
            return writeFile(`./data/${index}.txt`, sentence);
        });
        const sentenceFilePaths = await Promise.all(sentencesFilesPromises);

        console.log(sentenceFilePaths);

        await sentenceFilePaths.map((path) => {
            return appendNewLineToFile('filenames.txt', path);
        });

        console.log('sentence files appended to filenames.txt');

        // 4. Read the new files, sort the content, write it out to a new file, called sorted.txt. Store the name of the new file in filenames.txt

        sortedData = sortString(sentences);
        await writeFile("sorted.txt", sortedData);

        await appendNewLineToFile("filenames.txt", "./sorted.txt");

        // 5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list concurrently.
        await deleteFiles("filenames.txt");

        // empty filenames.txt at end

        await writeFile("filenames.txt", "");


    } catch (e) {
        console.error('Error: ', e);
    }
}
main();
